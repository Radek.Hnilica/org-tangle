#!/bin/bash
# $Id: cycle.sh,v 1.5 2022-11-09 17:47:31+01 radek Exp $
# $Source: /home/radek/st/src/org-tangle/src/cycle.sh,v $

typeset -r TIME_STAMP=mark.ts
typeset -r TITLE=org-tangle
typeset -r BIG_LOOP="10 minutes ago"
typeset -r LOOP=1m

while :; do
	# Check the time from last make
	if [[ $(date +%F_%T -r $TIME_STAMP) < $(date +%F_%T -d "$BIG_LOOP") ]]; then
		title "$TITLE building"
		make
		touch $TIME_STAMP
	fi

	echo -n " $(date +%T)"
	title "$TITLE sleeping"
	sleep $LOOP
done
