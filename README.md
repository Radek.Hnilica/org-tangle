# org-tangle

Tangle program for Emacs org files

Please see [org-tangle.org](https://gitlab.com/Radek.Hnilica/org-tangle/-/blob/main/org-tangle.org) instead.

## Building
The program is built by provided Makefile.  However, you need
org-tangle binary already to process the org-tangle.org to make
org-tangle binary.  To solve this chicken - egg problem a bootstrap C
file (org-tangle_bootstrap.c) is provided.  You simply:

```
gcc org-tangle_bootstrap.c -o org-tangle
```

Then copy this bootstrap version where your Makefille needs it, and
run `make`.`

## Public
The weaved document is published as
[pdf](https://radek-hnilica.gitlab.io/org-tangle/org-tangle.pdf),
[html](https://radek-hnilica.gitlab.io/org-tangle/org-tangle.html),
and plain
[txt](https://radek-hnilica.gitlab.io/org-tangle/org-tangle.txt).
