#line 1 "org-tangle.org"
#line 156
/*
 * Program Org Tangle for tangling .org files.
 * Author: Radek Hnilica
 */
#line 136
#define VERSION "0.0.9"
#line 1013
#define FAIL(format, ...) do { \
	fprintf(stdout, "FAIL(%d,%s) " format "\n",			\
		__LINE__, __func__, __VA_ARGS__);			\
	exit (1);							\
	} while (0)
#line 1072
#define DBG(format, ...) fprintf(stdout, "DBG(%d,%s) " format "\n", \
				 __LINE__, __func__, __VA_ARGS__)
#line 1079
#define DBGMSG(message)	fprintf(stdout, "DBG(%d,%s) %s\n", \
				__LINE__, __func__, message)
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#line 575
/* Line or list of lines */
typedef struct lines_s lines_t;
struct lines_s {
	lines_t *next;   /* linked list */
	int	lineno;	 /* line number in original org file */
	char    *text;   /* content of the line without trailing \n */
};
#line 301
typedef struct chunks_s chunks_t;
struct chunks_s {
	chunks_t *next;      /* next in linked list */
	char     *lang;      /* programming language the chunk is coded in */
	char     *name;      /* name / identifier */
	lines_t  *lines;     /* first line in linked list of lines */
	lines_t  *last_line; /* need for extending chunk */
	bool     is_root;    /* root chunk flag */
	char     *file_name; /* name of the file, mandatory for root chunk */
	bool     no_line;    /* do not produce #line directive */
};

/* Global Variables */
#line 327
chunks_t *chunk_list;		/* list of all chunks in the file */
#line 355
char    lbuf[256];	       /* buffer for source code block line */
int     lineno = 0;	       /* source line number in '.org' file */

/* Function Prototypes */
#line 332
void read_org_file(char * file_name);
#line 397
chunks_t *parse_begin_src(char *line);
#line 587
void read_lines(FILE *f, chunks_t *chunk);
#line 723
void tangle_file(char *file_name);
#line 767
void tangle_root_chunk(chunks_t *chunk, char *org_file_name);
#line 813
void tangle_chunk(chunks_t *chunk, char *indent, FILE *f);
#line 846
void tangle_chunk_by_name(char *name, char *indent, FILE *f);
#line 874
chunks_t *find_chunk(char *name);
#line 905
void tangle_line(lines_t *line, char *indent, FILE *f);
#line 968
char* rtrim(char* string, char junk);
#line 973
char* rtrim(char* string, char junk)
{
	char* original = string + strlen(string);
	while (*--original == junk); /* look for first non junk character */
	*(original + 1) = '\0';      /* replace last junk character */
	return string;
}
#line 985
const char *skip_blanks(const char *s);
#line 1024
void debug_print_chunk(chunks_t *chunk);
#line 1050
void debug_print_chunks(chunks_t *chunk);

#line 361
void
read_org_file(char * file_name)
{
	FILE     *ifh;           /* input file handle */
	chunks_t *chunk = NULL;  /* newly read chunk */

	if ((ifh = fopen(file_name, "r")) == NULL) {
		fprintf(stderr, "Can't open input file '%s'.  %s.\n",
			file_name, strerror(errno));
		exit(1);
	}
	lineno = 0;		/* reset line conter */
	while (!feof(ifh)) {
		memset(lbuf, 0, sizeof(lbuf)); /* paranoid cleanup */
		if (!fgets(lbuf, sizeof(lbuf), ifh)) break;
		lineno++;
		const char *t = skip_blanks(lbuf);
		if (strncasecmp(t, "#+begin_src", 11) == 0) {
			chunk = parse_begin_src(lbuf);
			read_lines(ifh, chunk);
		}
		else {  /* correct the reading of text line */
			if (lbuf[strlen(lbuf)-1] != '\n') {
				while (fgetc(ifh) != '\n');
			}
		}
	}
}
#line 488
bool
match_tangle(char *word, char **p, chunks_t *ch)
{
	if (strcmp(word, ":tangle") != 0) {
		return false;
	}
	/* get :tangle argument */
	char *arg;
	if ((arg = strsep(p, " \t"))) {
		ch->file_name = strdup(arg);
		ch->is_root = true;
		if (strcmp(ch->file_name, "no") == 0) {
			ch->is_root = false;
		}
	}
	else {
		fprintf(stderr, "ERR(%d) :tangle missing argument!"
			"  On line %d.\n"
			, __LINE__, lineno);
	}
	return true;
}
#line 514
bool
match_noweb_ref(char *word, char **p, chunks_t *ch)
{
	if (strcmp(word, ":noweb-ref") != 0)
		return false;

	/* get :noweb-ref argument */
	char *arg;
	if ((arg = strsep(p, " \t")))
		/* The argument should not start with ':'! */
		if (arg[0] != ':')
			ch->name = strdup(arg);
		else {	
			fprintf(stderr, "ERR(%d) :noweb-ref wrong chunk name '%s'!"
				"  On line %d.\n", __LINE__, arg, lineno);
		}
	else {
		fprintf(stderr, "ERR(%d) :noweb-ref missing chunk name!"
			"  On line %d.\n", __LINE__, lineno);
	}
	return true;
}
#line 540
bool
match_exports(char *word, char **p, __attribute__ ((unused)) chunks_t *ch)
{
	if (strcmp(word, ":exports") != 0)
		return false;

	/* has one argument */
	char *arg;
	if ((arg = strsep(p, " \t")) == NULL)
		fprintf(stderr, "ERR(%d) :exports "
			" missing argument on line %d.\n",
			__LINE__, lineno);

	if (arg[0] == ':') {
		fprintf(stderr, "ERR(%d) :exports "
			" have wrong argument '%s' on line %d.\n",
			__LINE__, word, lineno);
	}
	return true;
}
#line 401
chunks_t *parse_begin_src(char *line)
{
	rtrim(line, '\n');
	/* if (isblank(*line)) */
	/* 	DBG("('%s') src:%d", line, lineno); */
	chunks_t ch = {0};	/* temporary chunk structure */

	/* Define pointers used in parsing via strsep(). */
	char *p = line;
	char *word = NULL;	/* parsed word / token */

	//while (isblank(*p)) p++;  /* skip the blanks */
	/*MARK*/
#line 480
	while (isblank(*p)) p++;  /* skip the blanks */
	word = strsep(&p, " \t"); /* skip the #+begin_src */
	word = strsep(&p, " \t"); /* parse language identifier */
	ch.lang = strdup(word);	  /* remember the language */

	/*
	 * Look for variable names and process these mostly by
	 * matching functions.
	 */
	while ((word = strsep(&p, " \t"))) {
		if (match_tangle(word, &p, &ch)) continue;
		if (match_noweb_ref(word, &p, &ch)) continue;
		/* The :no-line is processed here */
		if (strcmp(word, ":no-line") == 0) {
			ch.no_line = 1;
			continue;
		}
		/* Ignore some of the org-mode keywords */
		if (match_exports(word, &p, &ch)) continue;
		/* Report unhandled keywords */
		fprintf(stderr, "INFO(%d) ignoring word '%s' on line %d.\n",
			__LINE__, word, lineno);
	}

	chunks_t *chunk = NULL;

	/* Do we have chunk name? */
	if (ch.name) {
		/* Lets try to look it in already read chunks */
		if ((chunk = find_chunk(ch.name))) {
			/* We found it, so we are extending existing
			 * chunk.  So lets do some checking */
			if (ch.lang == NULL)
				fprintf(stderr, "WARN(%d, %s): chunk '%s' found as %p, on line %d do not have language!\n",
					__LINE__, __func__,
					chunk->name, (void*) chunk, lineno);
			if (strcmp(ch.lang, chunk->lang) != 0) {
				fprintf(stderr, "WARN(%d) programming language mismatch '%s' != '%s' on line %d!\n",
					__LINE__, ch.lang, chunk->lang, lineno);
			}
			/* assert(strcmp(ch.lang, chunk->lang)==0); */
			assert(strcmp(ch.name, chunk->name)==0);
			return chunk;
		}
		/* The chunk was not found, so we create one */
	}
	/* This chunk has no name, so it needs to be created */
	chunk = (chunks_t*) calloc(1, sizeof(chunks_t));
	if (!chunk) {
		fprintf(stderr, "ERR(%d): Can't allocate memory for chunk. %s.\n",
			__LINE__, strerror(errno));
		exit(1);
	}
	memcpy(chunk, &ch, sizeof(chunks_t)); /* copy known values */

	/* Because we created new chunk, we need to link it into chunk_list */
	chunk->next = chunk_list;
	chunk_list = chunk;
	return chunk;
}
#line 592
void
read_lines(FILE *f, chunks_t *chunk) {
#line 659
	bool first_line = true;
	char offset[80];	/* offset characters */
	while (!feof(f)) {
		memset(lbuf, 0, sizeof(lbuf));
		if (fgets(lbuf, sizeof(lbuf), f) == NULL) {
			/* we stump on EOF or error happen */
			fprintf(stderr, "Error reading chunk lines.  %s.\n",
				strerror(errno));
			exit (1);
		}
		lineno++;

		const char *t = skip_blanks(lbuf);
		if (strncasecmp(t, "#+end_src", 9) == 0) {
			return;	/* We found and ending keyword. */
		}

		else {
#line 675
			if (first_line) {
				char *s = lbuf;
				char *t = offset;
				while (isblank(*s)) {
					if (*s == ' ')
						*t++ = *s;
					s++;
				}
				*t = '\0';		/* terminate the offset*/
				first_line = false;
				//Primitive Debugging
				//fprintf(stderr, "Offset is %zu at source line %d\n", strlen(offset), lineno);
			}

			/* For empty line skip processing */
			if (! (lbuf[0] == '\n' && lbuf[1] == '\0')) {
				/* Unescaping ',' */
				if (*t == ',' && *(t+1) == '*') {
					/* shift the line back over ',' */
					memmove((void*)t, t+1, strlen(t+1));
				}
#line 693
				{
					char *o = offset;
					char *p = lbuf;
				
					//if (lbuf[0] == '\n' && lbuf[1] == '\0') return;
					/* On empty lines, no processing is needed. */
					if (strlen(lbuf) == 0) {
						fprintf(stderr, "INFO: Empty line %d\n", lineno);
						return;
					}
					/* For each offset character */
					for (; *o; o++) {
						/* Search for that offset character in the lbuf */
						char *s = strchr(p, *o);
						if (s) {
							memmove(s, s+1, strlen(s));
						}
						else {
							fprintf (stderr, "ERR: Fail to look up offset character on line %d!\n", lineno);
						}
					}
				}
			}

			/* Add the line to lines */
			lines_t *line; /* actually read line */
			rtrim(lbuf, '\n'); // strip ending \n
			line = (lines_t*) calloc(1, sizeof(lines_t));
			line->text = strndup(lbuf, sizeof(lbuf));
			line->lineno = lineno;
			line->next = NULL;

			/* Append read line at end of lines */
			if (chunk->lines == NULL) { /* This is first line */
				chunk->lines = chunk->last_line = line;
			}
			else {
				/* append to end of list */
				chunk->last_line->next = line;
				chunk->last_line = line;
			}
		}
	}
	/*TBD: EOF reached in code chunk, rise an error! */
}
#line 743
void
tangle_file(char *file_name)
{
	/* fprintf(stderr, "TRACE(%d): %s(%s)\n", */
	/* 	__LINE__, __func__, file_name); */

	chunk_list = NULL; /* Be sure the list is empty before reading file */
	read_org_file(file_name);
	//fprintf(stderr, "DBG(%d):%s(): chunks %p chunks are read. See:\n", __LINE__, __func__, (void *) chunk_list);
	//debug_print_chunks(chunk_list);

	/* fprintf(stderr, "DBG(%d):%s(): Traversing all chunks looking for root ones.\n", */
	/* 	__LINE__, __func__); */
	for (chunks_t *chunk = chunk_list; chunk != NULL; chunk=chunk->next) {
		if (chunk->is_root) {
			tangle_root_chunk(chunk, file_name); // tangle_chunk
		}
	}
	/*TBD: free_chunks(chunks); */
}
#line 771
void
tangle_root_chunk(chunks_t *chunk, char *org_file_name)
{
	assert(chunk);
	assert(chunk->file_name);

	FILE *f;
	/* fprintf(stderr, "TRACE(%d): %s(*%p)\n", */
	/* 	__LINE__, __func__, (void*)chunk); */

	assert(chunk);
	assert(chunk->file_name);
	/* fprintf(stderr, "DBG(%d):%s() chunk->file_name: %s\n", */
	/* 	__LINE__, __func__, chunk->file_name); */

	/* char filepath[200]; */
	/* memset(filepath, 0, sizeof(filepath)); */
	/* strcat(filepath, "tmp/"); */
	/* strcat(filepath, chunk->file_name); */

	/* fprintf(stderr, "DBG(%d):%s() filepath: %s\n", */
	/* 	__LINE__, __func__, filepath); */
	f = fopen(chunk->file_name, "w");
	if (f == NULL) {
		fprintf(stderr, "ERR: Can't open output file %s. %s.\n",
			chunk->file_name, strerror(errno));
		exit (1);
	}
	if (!chunk->no_line) {
		fprintf(f, "#line 1 \"%s\"\n", org_file_name);		
	}
	tangle_chunk(chunk, "", f);
	fclose(f);
}
#line 827
void
tangle_chunk(chunks_t *chunk, char *indent, FILE *f)
{
	lines_t *line;	      /* chunk line to process */
	int     lineno = 0;   /* line number from original org file */

	for (line = chunk->lines; line; line = line->next) {
		/* do we need emit #line directive? */
		if (!chunk->no_line && line->lineno != lineno+1) {
			fprintf(f, "#line %d\n", line->lineno);
		}
		tangle_line(line, indent, f);
		lineno = line->lineno;
	}
}
#line 850
void
tangle_chunk_by_name(char *name, char *indent, FILE *f)
{
	//fprintf(stderr, "TRACE(%d):%s(%s,%p)\n", __LINE__, __func__, name, (void*) f);
	chunks_t *chunk = find_chunk(name);
	/* fprintf(stderr, "TRACE(%d):%s(%s,%p) chunk:%p\n", */
	/* 	__LINE__, __func__, name, (void*)f, (void*)chunk); */
	if (chunk) {
		//fprintf(stderr, "DBG: chunk %s found as %p\n", name, (void*) chunk);
	}
	else {
		fprintf(stderr, "ERR(%d): chunk '%s' not found!  On line %d.\n",
			__LINE__, name, lineno);
		return;
	}
	tangle_chunk(chunk, indent, f);
}
#line 910
void
tangle_line(lines_t *line, char *indent, FILE *f) {
	char *p = line->text;
	char *t = NULL;
	char *p0 = p;		/* Remember the start of the line */
	while (isblank(*p) && *p != '\0') p++; /* Skip all blanks */
	char *p1 = p;		/* ending position of indent */
	char *new_indent = NULL;

	if (strncmp(p, "{{", 2) == 0) {
		if ((t = strstr(p, "}}")) != NULL) {
			/* This line is chunk reference */
			// referred chunk name is between p+2 and t
			int l = t - p -2;
			//fprintf(stderr, "DBG: ref: '%.*s' l=%d\n", l, p+2, l);
			char *ref = strndup(p+2, l);
			//fprintf(stderr, "DBG: Chunk reference: '%s'\n", ref);
			// We need new indent by appending <p0..p>> to indent
			if (p1-p0 > 0) {
				//DBG("p1-p0 is %ld", p1-p0);
				/* Allocate space for new indent string */
				int slen = strlen(indent) + (p1-p0) + 1; /* compute needed memory */
				//DBG("need %d bytes for new indent", slen);
				if (!(new_indent = calloc(1, slen))) {
					FAIL("Can't allocate %d bytes of memory", slen);
				}
				memset(new_indent, 0, slen); /* be paranoid */
				strcat(new_indent, indent);
				strncat(new_indent, p0, p1-p0);
				//DBG("size of new_indent is %zd", strlen(new_indent));
				tangle_chunk_by_name(ref, new_indent, f);
				free(new_indent);
			}
			else
				tangle_chunk_by_name(ref, indent, f);
		}
	}
	else {
		fprintf(f, "%s%s\n", indent, line->text);
	}
}
#line 879
chunks_t
*find_chunk(char *name)
{
	//fprintf(stderr, "TRACE:%s(%s): chunk_list: %p\n", __func__, name, (void*) chunk_list);
	//assert(chunk_list);
	for (chunks_t *chunk = chunk_list; chunk != NULL; chunk = chunk->next) {
		//fprintf(stderr, "TRACE(%d): chunk: %p\n", __LINE__, (void*)chunk);
		//debug_print_chunk(chunk);
		if (chunk->is_root) continue;
		if (!chunk->name) continue;
		assert(chunk->name);
		if (strcmp(chunk->name, name) == 0) {
			//fprintf(stderr, "\tfound %p\n", (void*) chunk);
			return chunk;			
		}
		if (!chunk->next) return NULL;
	}
	return NULL;
}
#line 990
inline const char *skip_blanks(const char *t)
{
	while (isblank(*t)) t++;
	return t;
}
#line 1028
void
debug_print_chunk(chunks_t *chunk)
{
	fprintf(stderr, "DBG(%d):%s(*%p) \n",
		__LINE__, __func__, (void*)chunk);
	fprintf(stderr, "CHUNK: '%s' ", chunk->name);
	if (chunk->is_root) {
		fprintf(stderr, "is root to file '%s'", chunk->file_name);
	}
	fprintf(stderr, ", name: '%s'", chunk->name);
	fprintf(stderr, ", file: '%s'", chunk->file_name);
	fprintf(stderr, ", lang: '%s'", chunk->lang);
	fprintf(stderr, ", next: %p", (void*)chunk->next);
	fprintf(stderr, ", lines: %p", (void*)chunk->lines);
	fprintf(stderr, "\n");
	for(lines_t *line = chunk->lines; line; line = line->next) {
		fprintf(stderr, "%d:%s\n", line->lineno, line->text);
	}
}
#line 1054
void
debug_print_chunks(chunks_t *chunks)
{
	for (chunks_t *chunk = chunks; chunk; chunk = chunk->next) {
		debug_print_chunk(chunk);
	}
}

#line 212
int
main(int argc, char *argv[])
{
	puts("tangle v" VERSION " $Revision: 1.11 $ built " __DATE__ " " __TIME__);
	puts("__TIMESTAMP__:" __TIMESTAMP__);
	if (argc != 2) {
		fprintf(stderr, "Missing file name of org file! argc=%d\n", argc);
		exit(1);
	}
	//file_name = argv[1];

	tangle_file(argv[1]);
	//*TBD:
	// For Each file_name In argv Do
	//	tangle_file(file_name)
	// Done
	return 0;
}
