# ! make
# $Id: Makefile,v 1.11 2024-02-02 12:57:30+01 radek Exp $
# $Source: /home/radek/st/repo/sc/src/org-tangle/src/Makefile,v $

# Get the system os we are running on
SYSTEM=$(shell uname -s)

# On MacBook I'm using different compiler and need to define it there.
ifeq ($(SYSTEM),Darwin)
CC	= gcc-12
endif

STD	:= -std=c11 -g
STACK	:=
WARNS	:= -Wall -Wextra -pedantic
CFLAGS	:= $(STD) $(STACK) $(WARNS)

# Be carefull which tangle you use.  For each important version there
# is copy of the binary so you can get out of the situation where you
# lock yourself in in breaking the source code.
#ORG_TANGLE=./org-tangle-0.0.4
ORG_TANGLE=./org-tangle-0.1.0
#ORG_TANGLE = org-tangle
ORG_WEAVE = org-weave

SOURCES	= org-tangle.c
OBJECTS	= $(SOURCES:.c=.o)
EXECUTABLE = org-tangle
orgs	= $(wildcard *.org)	# Documents are recognised by '.org' extension
pdfs	= $(orgs:.org=.pdf)
htmls	= $(orgs:.org=.html)
txts	= $(orgs:.org=.txt)
outputs = $(addprefix $(output)/pdf/,$(pdfs)) $(addprefix $(output)/txt/,$(txts))
#outputs = $(addprefix $(output)/,$(pdfs)) $(addprefix $(output)/,$(txts))

output= $(HOME)/st/doc
#output= public

VPATH = $(output)
PANDOC_OPTS	= --tab-stop=8
PANDOC_PDF_OPTS = $(PANDOC_OPTS) -V geometry:a4paper,margin=2cm --number-sections --toc
# I have different installation on MacBook so need to handle pandoc options.
ifeq ($(SYSTEM),Darwin)
PANDOC_PDF_OPTS += --pdf-engine xelatex
else
PANDOC_PDF_OPTS += --highlight-style tango
endif
# The tango style fails on host factory.  Remove it when running on that host.

# Options for one page HTML.
PANDOC_HTML_OPTS = $(PANDOC_OPTS) -t html -N
PANDOC_HTML_OPTS += -c styling.css
PANDOC_HTML_OPTS += -s
PANDOC_HTML_OPTS += --toc --toc-depth=3 --top-level-division=chapter


default: all

all:	$(SOURCES) $(EXECUTABLE) $(outputs) $(output)/html/org-tangle.html

clean:
	rm -v $(OBJECTS) $(EXECUTALBE) Makefile.deps *~ $(pdfs) $(txts)

install: $(EXECUTABLE)
	install -m 0500 $(EXECUTABLE) $(HOME)/.local/bin

org-tangle.c : org-tangle.org
	$(ORG_TANGLE) org-tangle.org

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@



-include Makefile.deps
Makefile.deps:
	$(CC) $(CFLAGS) -MM *.[c] > Makefile.deps

### Rule to weave the .org file to .worg file
%.worg: %.org
	$(ORG_WEAVE) -f -o $@ $<

### Pandoc Rules for worg files
# NOTICE that I supplied '--from=org' option as pandoc do not
# necesarily recognize '.worg' files.

$(output)/txt/%.txt: %.worg
	pandoc $(PANDOC_TXT_OPTS) --from=org -o $@ $<
	cp $@ public/

$(output)/pdf/%.pdf: %.worg
	pandoc $(PANDOC_PDF_OPTS) --from=org -o $@ $<
	cp $@ public/

$(output)/html/%.html: %.worg
	pandoc $(PANDOC_HTML_OPTS) --from=org -o $@ $<
	cp $@ styling.css public/


# ORG to TXT (UTF-8) transformation
# %.txt: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-ascii-export-to-ascii

# # ORG to PDF usign LaTeX export
# %.pdf: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-latex-export-to-pdf
# 	rm -v $(basename $@).tex

# # ORG to HTML
# %.html: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-html-export-to-html

#Eof:$Id: Makefile,v 1.11 2024-02-02 12:57:30+01 radek Exp $
